'use client'
import {useRouter, useSearchParams} from "next/navigation";
import {useState} from "react";

export default function AgePage() {
    const [age, setAge] = useState('')
    const router = useRouter()
    const searchParams = useSearchParams()
    const name = searchParams.get('name')

    const handleSubmit = (e) => {
        e.preventDefault();
        router.push(`/genderPage?name=${name}&age=${age}`);
    }

    const handleAgeChange = (e) => {
        setAge(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className='text-center'>
                <div className='text-4xl mt-10'>당신의 나이를 입력해주세요</div>
                <label>
                    <input type='text' value={age} onChange={handleAgeChange} className='border-2 mt-10 p-4'/>
                </label>
                <div className='mt-4'>
                    <button className='border-2 py-2 px-4' type="submit">다음</button>
                </div>
            </div>
        </form>
    )
}