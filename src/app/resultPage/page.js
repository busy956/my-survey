'use client'
import {useSearchParams} from "next/navigation";

export default function ResultPage() {
    const searchParams = useSearchParams()
    const name = searchParams.get('name')
    const age = searchParams.get('age')
    const gender = searchParams.get('gender')

    return (
        <div className='text-center'>
            <div className='text-4xl my-10'>결과</div>
            <div className='mt-4'>당신의 이름 : {name}</div>
            <div className='mt-4'>당신의 나이 : {age}</div>
            <div className='mt-4'>당신의 성별 : {gender}</div>
        </div>
    )
}