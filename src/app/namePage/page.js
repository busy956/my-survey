'use client'
import {useRouter} from "next/navigation";
import {useState} from "react";

export default function NamePage() {
    const [name, setName] = useState("");
    const router = useRouter();

    const handleSubmit = (e) => {
        e.preventDefault();
        router.push(`/agePage?name=${name}`);
    }

    const handleNameChange = (e) => {
        setName(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className='text-center'>
                <div className='text-4xl mt-10'>당신의 이름을 입력해주세요</div>
                <label>
                    <input type='text' value={name} onChange={handleNameChange} className='border-2 mt-10 p-4'/>
                </label>
                <div className='mt-4'>
                    <button className='border-2 py-2 px-4' type="submit">다음</button>
                </div>
            </div>
        </form>
    )
}