'use client'
import {useRouter, useSearchParams} from "next/navigation";
import {useState} from "react";

export default function GenderPage() {
    const [gender, setGender] = useState('');
    const router = useRouter();
    const searchParams = useSearchParams()
    const name = searchParams.get('name')
    const age = searchParams.get('age')

    const handleSubmit = (e) => {
        e.preventDefault();
        router.push(`/resultPage?name=${name}&age=${age}&gender=${gender}`)
    }

    const handleGenderChange = (e) => {
        setGender(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            <div className='text-center'>
                <div className='text-4xl mt-10'>당신의 성별을 골라주세요</div>
                <label>
                    <select value={gender} onChange={handleGenderChange}>
                        <option value="">성별을 선택해주세요</option>
                        <option value='male'>남성</option>
                        <option value='female'>여성</option>
                    </select>
                </label>
                <div className='mt-4'>
                    <button className='border-2 py-2 px-4' type="submit">다음</button>
                </div>
            </div>
        </form>
    )
}